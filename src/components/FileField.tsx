import moment from "moment";
import React from "react";
import { Data } from "./Body";
import "../css/FileField.css";

interface attachmentFieldProps {
  data: Data;
}

export const AttachmentField: React.FC<attachmentFieldProps> = (
  props: attachmentFieldProps
) => {
  const attachmentData = props.data;
  const sender = attachmentData.senderType;
  const file = attachmentData.content.file;
  const date = moment(attachmentData.createdAt).format("LLL");

  return (
    <>
      {sender === "agent" && (
        <div className="Agent">
          <div className="FileField">
            <div className="Text">{sender}</div>
            <div className="FileContainer">
              <a className="File" download href={file}>
                Attachment: Click to download!
              </a>
            </div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
      {sender === "user" && (
        <div className="User">
          <div className="FileField">
            <div className="Text">{sender}</div>
            <div className="FileContainer">
              <a className="File" download href={file}>
                Attachment: Click to download!
              </a>
            </div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
    </>
  );
};
