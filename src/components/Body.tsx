import React from "react";
import "../css/Body.css";
import { AttachmentField } from "./FileField";
import { ImageField } from "./ImageField";
import { TextField } from "./TextField";

export interface Data {
  id: string;
  content: {
    text?: string;
    url?: string;
    file?: string;
  };
  contentType: string;
  senderType: string;
  createdAt: number;
}

interface BodyProps {
  data: Data[];
}

export const Body: React.FC<BodyProps> = (props: BodyProps) => {
  const conversation = props.data;

  return (
    <div className="Body">
      {conversation[0] &&
        conversation.map((data: Data, index: number) => {
          switch (data.contentType) {
            case "text":
              return <TextField key={index} data={data} />;
            case "image":
              return <ImageField key={index} data={data} />;
            case "attachment":
              return <AttachmentField key={index} data={data} />;
          }
        })}
    </div>
  );
};
