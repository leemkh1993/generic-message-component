import React from "react";
import { Data } from "./Body";
import "../css/ImageField.css";
import moment from "moment";

interface ImageFieldProps {
  data: Data;
}

export const ImageField: React.FC<ImageFieldProps> = (
  props: ImageFieldProps
) => {
  const imagesData = props.data;
  const sender = imagesData.senderType;
  const url = imagesData.content.url;
  const date = moment(imagesData.createdAt).format("LLL");

  return (
    <>
      {sender === "agent" && (
        <div className="Agent">
          <div className="ImageField">
            <div className="Text">{sender}</div>
            <div className="ImageContainer">
                <img src={url} alt="image" className="Image"/>
            </div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
      {sender === "user" && (
        <div className="User">
          <div className="ImageField">
            <div className="Text">{sender}</div>
            <div className="ImageContainer">
                <img src={url} alt="image" className="Image"/>
            </div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
    </>
  );
};
