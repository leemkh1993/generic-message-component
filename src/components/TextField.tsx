import React from "react";
import { Data } from "./Body";
import "../css/TextField.css";
import moment from "moment";

interface TextFieldProps {
  data: Data;
}

export const TextField: React.FC<TextFieldProps> = (props: TextFieldProps) => {
  const textData = props.data;
  const sender = textData.senderType;
  const content = textData.content.text;
  const date = moment(textData.createdAt).format("LLL");

  return (
    <>
      {sender === "agent" && (
        <div className="Agent">
          <div className="TextField">
            <div className="Text">{sender}</div>
            <div className="Text">{content}</div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
      {sender === "user" && (
        <div className="User">
          <div className="TextField">
            <div className="Text">{sender}</div>
            <div className="Text">{content}</div>
            <div className="Text">{date}</div>
          </div>
        </div>
      )}
    </>
  );
};
