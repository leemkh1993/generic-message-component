import React from 'react';
import './App.css';
/** Conversation Data */
import conversationData from './fixtures/conversation';
import { Body } from './components/Body';

function App() {
  return (
    <div className="App">
      <Body data={conversationData}/>
    </div>
  );
}

export default App;
